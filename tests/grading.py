import json
from utils import *


score = 0.0

with open("./logs/output.json", "r") as file:
    test_scores = {
        'test_mse_derivative': 0.25,
        'test_mae_derivative': 0.25,
        'test_l2_reg_derivative': 0.15,
        'test_l1_reg_derivative': 0.15,
        'test_mse': 0.05,
        'test_mae': 0.05,
        'test_l2_reg': 0.05,
        'test_l1_reg': 0.05

    }
    res = json.load(file)
    tests = res["content"]["suites"]["0"]["tests"]
    for test_num in tests:
        if tests[test_num]["status"] == "PASS":
            score += test_scores[tests[test_num]["test_name"]]
    print("Final:", score)