import os
import sys

import numpy as np

sys.path.insert(0, os.path.abspath('./'))
from src.loss_and_derivatives import LossAndDerivatives

class TestLossAndDerivatives():
    ref_dict = np.load(os.getenv("PUBLIC_DATA_PATH"), allow_pickle=True).item()
    X_ref = ref_dict['X_ref']
    y_ref = ref_dict['y_ref']
    w_hat = ref_dict['w_hat']

    def test_mse_derivative(self):
        mse_derivative = LossAndDerivatives.mse_derivative(self.X_ref, self.y_ref, self.w_hat)
        assert np.allclose(mse_derivative, self.ref_dict['mse_derivative'], atol=1e-4), \
            "Something wrong with mse derivative"

    def test_mae_derivative(self):
        mae_derivative = LossAndDerivatives.mae_derivative(self.X_ref, self.y_ref, self.w_hat)
        assert np.allclose(mae_derivative, self.ref_dict['mae_derivative'], atol=1e-4), \
            "Something wrong with mae derivative"

    def test_l2_reg_derivative(self):
        l2_reg_derivative = LossAndDerivatives.l2_reg_derivative(self.w_hat)
        assert np.allclose(l2_reg_derivative, self.ref_dict['l2_reg_derivative'], atol=1e-4), \
            "Something wrong with l1 reg derivative"

    def test_l1_reg_derivative(self):
        l1_reg_derivative = LossAndDerivatives.l1_reg_derivative(self.w_hat)
        assert np.allclose(l1_reg_derivative, self.ref_dict['l1_reg_derivative'], atol=1e-4), \
            "Something wrong with l2 reg derivative"

    def test_mse(self):
        mse = LossAndDerivatives.mse(self.X_ref, self.y_ref, self.w_hat)
        assert np.allclose(mse, self.ref_dict['mse'], atol=1e-4), \
            "Something wrong with mse"

    def test_mae(self):
        mae = LossAndDerivatives.mae(self.X_ref, self.y_ref, self.w_hat)
        assert np.allclose(mae, self.ref_dict['mae'], atol=1e-4), \
            "Something wrong with mae"

    def test_l2_reg(self):
        l2_reg = LossAndDerivatives.l2_reg(self.w_hat)
        assert np.allclose(l2_reg, self.ref_dict['l2_reg'], atol=1e-4), \
            "Something wrong with l2 reg"

    def test_l1_reg(self):
        l1_reg = LossAndDerivatives.l1_reg(self.w_hat)
        assert np.allclose(l1_reg, self.ref_dict['l1_reg'], atol=1e-4), \
            "Something wrong with l1 reg"
