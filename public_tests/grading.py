import json
import os
import yaml
import datetime
import pytz


def count_scores(score_file, report_file):
    with open(score_file, "r") as score, \
            open(report_file, 'r') as report:

        final_score = 0.0
        failed_tests = []

        scores = yaml.load(score, Loader=yaml.FullLoader)
        result = json.load(report)
        tests = result["content"]["suites"];
        for suit in tests:
            for test_num in tests[suit]["tests"]:
                if tests[suit]["tests"][test_num]["status"] == "PASS":
                    test_name = tests[suit]["tests"][test_num]["test_name"]
                    final_score += scores[test_name]
                else:
                    failed_tests.append((test_num, tests[suit]["tests"][test_num]["test_name"]))

        return final_score, failed_tests


def generate_report(output_file, score, failed_tests):
    time = datetime.datetime.now(tz=pytz.timezone('Europe/Moscow')).strftime("%d/%m/%Y %H:%M:%S")

    report = {"Student": os.getenv("GITLAB_USER_NAME"),
              "Time": time,
              "Merge_Request_link": os.getenv("CI_MERGE_REQUEST_SOURCE_PROJECT_URL") + "/-/merge_requests/" +
                                    os.getenv("CI_MERGE_REQUEST_IID"),
              "Grade": score,
              "Failed tests": failed_tests,
              "Task_name": os.getenv("CI_PROJECT_TITLE")}

    with open(output_file, "w") as file:
        json.dump(report, file)


if __name__ == "__main__":
    score, tests_failed = count_scores(os.getenv("GRADING_SCORES"), os.getenv("LOGS_PATH"))
    generate_report(os.getenv("OUTPUT_GRADE"), score, tests_failed)
